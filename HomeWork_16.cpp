// HomeWork_16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>

int main()
{
    const int N = 10;
    int arr [N][N];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++) arr[i][j] = i + j;

    //
    time_t t;
    struct tm timeinfo;
    int day;
    time(&t);
    localtime_s(&timeinfo, &t);
    day = timeinfo.tm_mday;

    //
    int ln = day % N, sum = 0;
    for (int j = 0; j < N; j++) sum += arr[ln][j];

    //
    std::cout << "N = " << N << '\n';
    std::cout << "-----\n";
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << (i == ln && j == 0? "*" : " ");
            std::cout << (arr[i][j] < 10 ? " " : "" ) << arr[i][j] << ' ';
        }
        if (i == ln)
        {
            std::cout << "* " << sum;
        }
        std::cout << '\n';
    }
    std::cout << "-----\n";

    //
    std::cout << "Curr date: " << day << '.' << (timeinfo.tm_mon+1) << '.' << (timeinfo.tm_year+1900) << '\n';
    std::cout << "ln = " << ln << "\n";
    std::cout << "sum = " << sum << '\n';

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
